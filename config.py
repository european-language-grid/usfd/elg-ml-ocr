import os

class Config(object):
    GATE_CLOUD_ENDPOINT = "https://cloud-api.gate.ac.uk/process"
    OCR_SLUG = "/ml-ocr"
    GCLOUD_OCR_API_KEY = os.environ.get("GCLOUD_OCR_API_KEY", "holder-api-key")
    GCLOUD_OCR_API_PASSWORD = os.environ.get("GCLOUD_OCR_API_PASSWORD", "holder-api-password")
