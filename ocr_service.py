import aiohttp

from elg import QuartService
from elg.model import TextsResponseObject
from elg.model.response import TextsResponse
from elg.quart_service import ProcessingError
from quart import current_app
from aiolimiter import AsyncLimiter

from loguru import logger


class OcrService(QuartService):

	async def setup(self):
		key = self.get_config("GCLOUD_OCR_API_KEY")
		password = self.get_config("GCLOUD_OCR_API_PASSWORD")
		auth = aiohttp.BasicAuth(key, password)
		self.session = aiohttp.ClientSession(auth=auth, raise_for_status=True)
		# rate limit to just under two RPS on average
		self.limiter = AsyncLimiter(59, 30)

	async def shutdown(self):
		if self.session is not None:
			await self.session.close()

	def convert_input(self, request):
		""" get relevant details from image request """
		logger.debug("Converting the incoming image request")

		try:
			is_url = True if request.type == "text" else False
			image = request.content
			img_format = "text" if request.type == "text" else request.format

			if not image:
				raise Exception

		except Exception as e:
			logger.error(e)
			message = "Your request must provide either a valid image url or valid image file"
			raise ProcessingError.InternalError(message)

		logger.debug("Converted request with is_url and image length being: {is_url}, {image}", is_url=is_url,
					 image=len(image))
		return is_url, image, img_format

	async def run_ocr(self, is_url, input, img_format):
		""" call gate cloud ocr code """
		logger.debug("Preparing to send request to gate cloud OCR service")

		ocr_endpoint = self.get_config("GATE_CLOUD_ENDPOINT") + self.get_config("OCR_SLUG")


		try:
			async with self.limiter:
				if is_url:
					do_request = self.session.get(ocr_endpoint, params={'url': input, 'script': 'loop'})
					logger.debug("Sending url based request to gate cloud OCR service")

				else:
					extra_headers = {"Content-Type": "image/" + img_format}
					do_request = self.session.post(ocr_endpoint, data=input, headers=extra_headers,
												params={'script': 'loop'})
					logger.debug("Sending image based request to gate cloud OCR service")

				async with do_request as result:
					return await result.json()

		except Exception as e:
			logger.error(e)
			message = "There has been an error calling the gate cloud ocr service. See logs for details."
			raise ProcessingError.InternalError(message)

	def convert_gate_output(self, gate_output):
		""" convert gate output to valid elg response """

		logger.debug("Converting the following to an elg response: {gate_output}", gate_output=gate_output)

		try:
			if gate_output.get("bounding_boxes") and len(gate_output.get("bounding_boxes")):
				boxes = []
				full_text = ""

				# handle bounding box section of output
				bounding_boxes = gate_output.get("bounding_boxes")
				for bbox in bounding_boxes:
					lang = bbox.get("language", dict())
					script = bbox.get("script", dict())
					text = bbox.get("text")
					features = {
						"bounding_box": bbox.get("bounding_box"),
						# promote the language code to a non-nested feature, to make life easier for the GUI
						"language_code": lang.get("code"),
						"language": lang,
						"script": script,
					}
					response_object = TextsResponseObject(role="chunk", content=text, features=features)
					boxes.append(response_object)
					full_text += (" " + text)

				# handle script section of output
				script = gate_output.get("script")
				features = {
					"script": script,
					"full_text": full_text
				}
				script_elg_response = TextsResponseObject(role="image", texts=boxes, features=features)
				elg_response = TextsResponse(texts=[script_elg_response])
			else:
				# No text found - send response that is successful, but empty
				elg_response = TextsResponse(texts=[])

		except Exception as e:
			logger.error(e)
			message = "There has been an error converting from the ocr response to the elg response. See logs for details."
			raise ProcessingError.InternalError(message)

		logger.debug("Converted to the following elg response: {elg_repsonse}", elg_repsonse=elg_response)
		return elg_response

	async def process_text(self, text_request):
		""" entire process: take in elg request -> call gate cloud ocr service -> convert to elg response """
		logger.info("Processing ocr image url request")
		is_url, input, img_format = self.convert_input(text_request)
		gate_output = await self.run_ocr(is_url, input, img_format)
		elg_output = self.convert_gate_output(gate_output)
		logger.info("Finished processing ocr request")
		return elg_output

	async def process_image(self, image_request):
		""" entire process: take in elg request -> call gate cloud ocr service -> convert to elg response """
		logger.info("Processing ocr image request")
		is_url, input, img_format = self.convert_input(image_request)
		gate_output = await self.run_ocr(is_url, input, img_format)
		elg_output = self.convert_gate_output(gate_output)
		logger.info("Finished processing ocr request")
		return elg_output

	def get_config(self, config_key):
		return current_app.config[config_key]


quart_service = OcrService("ml-ocr")
app = quart_service.app
app.config.from_object("config.Config")
